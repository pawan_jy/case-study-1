# Java Case Study One
A simple framework for Online Shopping Application to represent Prime Accounts
and Normal(non-prime) Accounts.

Using which to design application for GoShopping App.


## Objectives
- To understand the concept of framework in application development.
- Areas of application for Abstract classes, abstract methods etc.
- Polymorphism and its uses.
- Final fields and Lazy Initialization
- Getter and Setter methods
- Lazy Binding of methods


## Diagram
![Class Diagram](https://i.imgur.com/c80gPSJ.png)

## Abstract Classes and Interfaces For Framework
### Class ShopAcc -
| Fields  | Access  | Type   | Proprety   |
| ------- | ------- | ------ | ---------- |
| accNo   | private | int    | Read Only  |
| accNm   | private | String | Read Write |
| charges | private | float  | Read Only  |

| Constructors | Access    | Parameters            |
| ------------ | --------- | --------------------- |
|              | public    | accNo, accNm, charges |

| Methods     | Access | Return Type | Parameters | Particulars |
| ----------- | ------ | ----------- | -----------| ----------- |
| bookProduct | public | void        | float      |             |
| items       | public | void        | float      |             |
| toString    | public | String      |            | Overridden  |


### Class PrimeAcc extends ShopAcc -
| Fields          | Access  | Type    | Proprety  | Particulars  |
| --------------- | ------- | ------- | --------- | ------------ |
| isPrime         | private | boolean | Read Only |              |
| deleveryCharges | private | float   | Read Only | static final |

| Constructors | Access | Parameters                     |
| ------------ | ------ | ------------------------------ |
|              | public | AccNo, accNm, charges, isPrime |

| Methods     | Access | Return Type | Parameters | Particulars |
| ----------- | ------ | ----------- | -----------| ----------- |
| bookProduct | public | void        | float      | Overridden  |
| toString    | public | String      |            | Overridden  |


### Class NormalAcc extends ShopAcc -
| Fields          | Access  | Type  | Proprety  | Particulars |
| --------------- | ------- | ----- | --------- | ----------- |
| deleveryCharges | private | float | Read Only | final       |

| Constructors | Access | Parameters                             | Particulars                         |
| ------------ | ------ | ---------------------------------------| ----------------------------------- |
|              | public | AccNo, accNm, charges, deliveryCharges | Lazy Initialization for creditLimit |

| Methods     | Access | Return Type | Parameters | Particulars |
| ----------- | ------ | ----------- | -----------| ----------- |
| bookProduct | public | void        | float      | Overridden  |
| toString    | public | String      |            | Overridden  |


### Interface ShopFactory -
| Methods         | Access | Return Type | Parameters                             |
| --------------- | ------ | ----------- | -------------------------------------- |
| getNewPrimeAcc  | public | PrimeAcc    | accNo, accNm, charges, isPrime         |
| getNewNormalAcc | public | NormalAcc   | accNo, accNm, charges, deliveryCharges |



## Concrete Classes and Interfaces For Application
### Class GSPrimeAcc extends Abstract PrimeAcc -
| Fields  | Access  | Type    | Proprety  | Particulars  |
| --------| ------- | ------- | --------- | ------------ |
| charges | private | float   | Read Only | static final |

| Constructors | Access | Parameters                     |
| ------------ | ------ | ------------------------------ |
|              | public | AccNo, accNm, charges, isPrime |

| Methods     | Access | Return Type | Parameters | Particulars |
| ----------- | ------ | ----------- | -----------| ----------- |
| bookProduct | public | void        | float      | Overridden  |
| toString    | public | String      |            | Overridden  |


### Class GSNormalAcc extends Abstract NormalAcc -
| Constructors | Access | Parameters                             | Particulars                             |
| ------------ | ------ | ---------------------------------------| --------------------------------------- |
|              | public | AccNo, accNm, charges, deliveryCharges | Lazy Initialization for deliveryCharges |

| Methods     | Access | Return Type | Parameters | Particulars |
| ----------- | ------ | ----------- | -----------| ----------- |
| bookProduct | public | void        | float      | Overridden  |
| toString    | public | String      |            | Overridden  |


### Interface GSShopFactory -
| Methods         | Access | Return Type | Parameters                             |
| --------------- | ------ | ----------- | -------------------------------------- |
| getNewPrimeAcc  | public | PrimeAcc    | accNo, accNm, charges, isPrime         |
| getNewNormalAcc | public | NormalAcc   | accNo, accNm, charges, deliveryCharges |



## Main Class (entry point for application part)
- Assign instance of GSShopFactory to ShopFactory reference
- Instantiate GSPrimeAcc and refer it through reference PrimeAcc
- Instantiate GSNormalAcc and refer it through reference NormalAcc
- Invoke bookProduct() method
- Invoke toString() method



## Output
![Class Diagram](https://telegra.ph/file/482f55c0d89b14f48cffd.png)
