package com.goShopping.application;

import com.goShopping.framework.NormalAcc;
import com.goShopping.framework.PrimeAcc;
import com.goShopping.framework.ShopFactory;

public class Main {
    
    ShopFactory shpFac;
    PrimeAcc prAcc;
    NormalAcc nrmAcc;

    Main(){
        shpFac = new GSShopFactory();
        prAcc = shpFac.getNewPrimeAcc(1, "accOne", 12, true);
        nrmAcc = shpFac.getNewNormalAcc(2, "accTwo", 15, 0.5F);
    }
    public static void main(String[] args) {
        Main mainApp = new Main();

        mainApp.prAcc.bookProduct(0.1F);
        mainApp.nrmAcc.bookProduct(0.2F);

        System.out.println(mainApp.prAcc.toString());
        System.out.println(mainApp.nrmAcc.toString());
    }
}
