package com.goShopping.application;

import com.goShopping.framework.NormalAcc;

public class GSNormalAcc extends NormalAcc {

    public GSNormalAcc(int accNo, String accNm, float charges, float deliveryCharges) {
        super(accNo, accNm, charges, deliveryCharges);
    }

    @Override
    public void items(float f) {
        // TODO Auto-generated method stub
        System.out.println(String.format("\nitems method of concrete class GSNormalAcc called with float value %.2f", f));
    }

    @Override
    public void bookProduct(float f) {
        // TODO Auto-generated method stub
        System.out.println(String.format("\nbookProduct method of concrete class GSNormalAcc called with float value %.2f", f));
    }

    @Override
    public String toString() {
        return String.format("\nA/c No: %s\nA/c Name: %s\nCharges: %.2f\nPrime Account: false\nDelivery Charges: %.2f\nCredit Limit: %.2f\n", getAccNo(), getAccNm(), getCharges() + getDeliveryCharges(), getDeliveryCharges(), getCreditLimit());
    }
}
