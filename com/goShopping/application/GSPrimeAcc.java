package com.goShopping.application;

import com.goShopping.framework.PrimeAcc;

public class GSPrimeAcc extends PrimeAcc {

    private static final float charges = 0;   //it's already in ShopAcc class (not static there)

    public GSPrimeAcc(int accNo, String accNm, float charges, boolean isPrime) {
        super(accNo, accNm, charges, isPrime);
    }

    @Override
    public void bookProduct(float f) {
        // TODO Auto-generated method stub
        System.out.println(String.format("\nbookProduct method of concrete class GSPrimeAcc called with float value %.2f", f));
    }

    @Override
    public String toString() {
        return String.format("\nA/c No: %s\nA/c Name: %s\nCharges: %.2f\nPrime Account: %s\nDelivery Charges: %.2f\n", getAccNo(), getAccNm(), getCharges() + getDeliveryCharges(), Boolean.toString(getIsPrime()), getDeliveryCharges());
    }

    @Override
    public void items(float f) {
        // TODO Auto-generated method stub
        System.out.println(String.format("\nitems method of concrete class GSPrimeAcc called with float value %.2f", f));
    }
}