package com.goShopping.framework;

public abstract class NormalAcc extends ShopAcc {

	// Variables
	private final float deliveryCharges = 0.05F;
	private float creditLimit;

	// Constructors
	public NormalAcc(){}
	public NormalAcc(int accNo, String accNm, float charges, float deliveryCharges) {
		super(accNo, accNm, charges);
	}

	// Getters and Setters
	public float getDeliveryCharges(){
		return deliveryCharges;
	}

	public float getCreditLimit(){
		this.creditLimit = (getCharges() * 0.75F);
		return creditLimit;
	}

	public void setCreditLimit(float creditLimit){
		this.creditLimit = creditLimit;
	}

	// Overridden methods from ShopAcc
	@Override
	public void bookProduct(float f){
		// TODO Auto-generated method stub
		System.out.println(String.format("bookProduct method of abstract class NormalAcc called with float value %.2f", f));
	};

	@Override
	public String toString(){
		return String.format("A/c No: %i\nA/c Name: %s\nCharges: %.2f\nPrime Account: false\nDelivery Charges: %.2f\nCredit Limit: %2f\n", getAccNo(), getAccNm(), getCharges() + getDeliveryCharges(), getDeliveryCharges(), getCreditLimit());
	};

}
