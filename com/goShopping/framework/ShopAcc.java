package com.goShopping.framework;

public abstract class ShopAcc {

	// Variables
	private int accNo;
	private String accNm;
	private float charges;

	// Constructors
	public ShopAcc(){}
	public ShopAcc(int accNo, String accNm, float charges) {
		this.accNo = accNo;
		this.accNm = accNm;
		this.charges = charges;
	}

	// Getters and Setters
	public int getAccNo(){
		return accNo;
	}
	public void setAccNo(int accNo){
		this.accNo = accNo;
	}

	public String getAccNm(){
		return accNm;
	}
	public void setAccNm(String accNm){
		this.accNm = accNm;
	}

	public float getCharges(){
		return charges;
	}
	public void setCharges(float charges){
		this.charges = charges;
	}

	// Abstract methods
	public abstract void bookProduct(float f);
	public abstract void items(float f);

	// Override toString method from Object class
	@Override
	public String toString(){
		return String.format("A/c No: %i\nA/c Name: %s\nCharges: %.2f\n", getAccNo(), getAccNm(), getCharges());
	};
}
