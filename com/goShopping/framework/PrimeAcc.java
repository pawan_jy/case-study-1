package com.goShopping.framework;

public abstract class PrimeAcc extends ShopAcc {

	// Variables
	private boolean isPrime;
	private static final float deliveryCharges = 0;

	// Constructors
	public PrimeAcc(){}
	public PrimeAcc(int accNo, String accNm, float charges, boolean isPrime) {
		super(accNo, accNm, charges);
		this.isPrime = isPrime;
	}

	// Getters and Setters
	public boolean getIsPrime(){
		return isPrime;
	}
	public void setIsPrime(boolean isPrime){
		this.isPrime = isPrime;
	}

	public float getDeliveryCharges(){
		return deliveryCharges;
	}

	// Overridden methods from ShopAcc
	@Override
	public  void bookProduct(float f){
		// TODO Auto-generated method stub
		System.out.println(String.format("bookProduct method of abstract class PrimeAcc called with float value %.2f", f));
	};

	@Override
	public String toString(){
		return String.format("A/c No: %i\nA/c Name: %s\nCharges: %.2f\nPrime Account: %s\nDelivery Charges: %.2f\n", getAccNo(), getAccNm(), getCharges() + getDeliveryCharges(), Boolean.toString(getIsPrime()), getDeliveryCharges());
	};

}
